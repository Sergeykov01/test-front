import {AxiosInstance} from 'axios'
import {ActionContext} from 'vuex'

interface state {
  device: string
  menu: Object
}

interface DeviceInterface {
  isMobile: boolean,
  isDesktop: boolean
}
interface NuxtApp {
  $device: DeviceInterface
}

interface Context {
  isDesktop: boolean,
  isMobile: boolean
  $axios: AxiosInstance
  app: NuxtApp
}
export const state = () => ({
  device: null,
  menu: null
})

export const mutations = {
  setDevice(state: state , value: string){
    state.device = value
  },
  storeMenu(state: state, value: Object){
    state.menu = value
  }
}

export const actions = {
  //@ts-ignore
  async nuxtServerInit({commit}, ctx: Context){
    let device: string = 'mkp'
    if (ctx.app.$device.isMobile){
      device = 'mobile'
    }else if (ctx.app.$device.isDesktop){
      device = 'desktop'
    }

    const {data} = await ctx.$axios.get('/v2/main_menu');

   commit('storeMenu', data)
    commit('setDevice', device)
  },

}
