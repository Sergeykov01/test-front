import {Vue} from 'nuxt-property-decorator'

export default class BaseComponent extends Vue{

  constructor(){
    super()
  }

  getComponent(){
    if (this.$options.components){
      const components: string[] = Object.keys(this.$options.components)
      const device: string = this.$store.state.device
      const index: number = components.findIndex(el => el === device)
      if (index >= 0) {
        return device
      }
      return 'desktop'
    }
  }



}
